<?php

/**
 * Class Template
 */
class Template {

    private $_Registry = null;
    public $twig = null;

    /**
     * Template constructor.
     * Init Twig template engine
     *
     * todo: load settings from ini file
     *
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;

        $loader = new Twig_Loader_Filesystem(__VIEW_PATH__);
        $this->twig = new Twig_Environment($loader, array(
            'cache' => false // __VAR_PATH__ . '/cache'
        ));
    }


}