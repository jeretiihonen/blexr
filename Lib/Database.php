<?php

/**
 * Class Database
 */
class Database {

    private static $instance = null;
    private $connection;

    /**
     * Database constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->connection = new PDO("mysql:host={$Registry->__get('settings')['database']['host']};dbname={$Registry->__get('settings')['database']['dbname']}", $Registry->__get('settings')['database']['user'],$Registry->__get('settings')['database']['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    }

    /**
     * @param $Registry
     * @return Database|null
     */
    public static function getInstance($Registry)
    {
        if(!self::$instance)
        {
            self::$instance = new Database($Registry);
        }

        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

}