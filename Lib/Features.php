<?php

/**
 * Class Features
 */
class Features {

    private $_Registry;

    /**
     * Features constructor.
     * @param $Registry
     */
    public function __construct($Registry){
        $this->_Registry = $Registry;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserFeatures($userId){
        $allFeatures = $this->fetchAllFeatures();
        $userFeatures = $this->fetchUserFeatures($userId);

        foreach ( $allFeatures as $featureId => $featureName ){

            foreach ( $userFeatures as $userFeaturesId ){

                if ( $userFeaturesId == $featureId ){
                    $return[$featureName .'-'. $featureId] = true;
                    continue 2;
                }

            }
            $return[$featureName .'-'. $featureId] = false;

        }

        return $return;
    }

    /**
     * Fetch all possible features from database
     * @return array
     */
    public function fetchAllFeatures(){
        $sql = "SELECT * FROM enabled_features ";
        $result = $this->_Registry->Database->getConnection()->query($sql);
        $return = array();

        while($feature = $result->fetch(PDO::FETCH_OBJ)) {
            $return[$feature->id] = $feature->name;
        }

        return $return;
    }

    /**
     * Fetch all features what user has
     *
     * @param $userId
     * @return array
     */
    public function fetchUserFeatures($userId){
        $sql = "SELECT * FROM enabled_features_users WHERE userId = ". $userId;
        $result = $this->_Registry->Database->getConnection()->query($sql);
        $return = array();

        while($userFeature = $result->fetch(PDO::FETCH_OBJ)) {
            $return[] = $userFeature->featureId;
        }

        return $return;
    }

    /**
     * Save feature to user
     *
     * @param $userId
     * @param $featureId
     */
    public function saveUserFeature($userId, $featureId){
        $sql = "INSERT INTO enabled_features_users (userId, featureId) VALUES ($userId, $featureId)";
        $this->_Registry->Database->getConnection()->exec($sql);
    }

    /**
     * Remove feature from user
     *
     * @param $userId
     * @param $featureId
     */
    public function removeUserFeature($userId, $featureId){
        $sql = "DELETE FROM enabled_features_users WHERE userId=". $userId ." AND featureId=". $featureId;
        $this->_Registry->Database->getConnection()->exec($sql);
    }

}