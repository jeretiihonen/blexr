<?php
// Import the necessary classes
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

// Setup a new Eloquent Capsule instance
$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => $Registry->__get('settings')['database']['driver'],
    'host'      => $Registry->__get('settings')['database']['host'],
    'database'  => $Registry->__get('settings')['database']['dbname'],
    'username'  => $Registry->__get('settings')['database']['user'],
    'password'  => $Registry->__get('settings')['database']['password'],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci'
]);

$capsule->bootEloquent();

