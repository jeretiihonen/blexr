<?php
/**
 * this is the base rgistry which stores all vars and objects needed across all calls
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

/**
 * Class Registry
 */
class Registry {
    
    private $_vars = array();

    /**
     * Registry constructor.
     */
    public function __construct() {
        if ( file_exists(__CONFIG_PATH__ . '/settings.ini') ) {
            $this->_vars['settings'] = parse_ini_file(__CONFIG_PATH__ . '/settings.ini', true);
        } else {
            die ("File settings.ini not found!");
        }

    }

    /**
     * @param $index
     * @param $value
     */
    public function __set($index, $value) {
        $this->_vars[$index] = $value;
    }

    /**
     * @param $index
     * @return bool|mixed
     */
    public function __get($index) {
        if(isset($this->_vars[$index])) {
            return $this->_vars[$index];
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function returnVariables() {
        return $this->_vars;
    }
}