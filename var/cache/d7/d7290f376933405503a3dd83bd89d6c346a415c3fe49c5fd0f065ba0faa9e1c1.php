<?php

/* index/index.phtml */
class __TwigTemplate_62c300debea1c233249833d7fdbbe20d942a84c682499054a5bfd25fbc75ac9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>

<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">

    <title>Blexr</title>

    <link rel=\"stylesheet\" href=\"static/css/default.css?v=1.0\">

</head>

<body>
<div id=\"siteContainer\">
    <div id=\"menuContainer\">



    </div>

    <div id=\"contentContainer\">

        ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "

    </div>
</div>

<script src=\"static/javascripts/jquery-1.12.3.min.js\"></script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "index/index.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 23,  19 => 1,);
    }
}
/* <!doctype html>*/
/* */
/* <html lang="en">*/
/* <head>*/
/*     <meta charset="utf-8">*/
/* */
/*     <title>Blexr</title>*/
/* */
/*     <link rel="stylesheet" href="static/css/default.css?v=1.0">*/
/* */
/* </head>*/
/* */
/* <body>*/
/* <div id="siteContainer">*/
/*     <div id="menuContainer">*/
/* */
/* */
/* */
/*     </div>*/
/* */
/*     <div id="contentContainer">*/
/* */
/*         {{ name }}*/
/* */
/*     </div>*/
/* </div>*/
/* */
/* <script src="static/javascripts/jquery-1.12.3.min.js"></script>*/
/* </body>*/
/* </html>*/
