<?php

// Import the necessary classes
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class AuthController
 */
class AuthController {

    /**
     * Try sentinel login
     */
    public function login() {
        try {
            if ( !Sentinel::authenticate([
                    'email' => $_POST['username'],
                    'password' => $_POST['password'],
            ]) ){
                $return = array(
                    'status' => 0,
                    'message' => 'Login failed!'
                );
            } else {
                $return = array(
                    'status' => 1,
                    'message' => 'Login successful'
                );
            }
        } catch (Cartalyst\Sentinel\Checkpoints\ThrottlingException $ex) {
            $return = array(
                'status' => 0,
                'message' => 'Too many attempts!'
            );
        } catch (Cartalyst\Sentinel\Checkpoints\NotActivatedException $ex){
            $return = array(
                'status' => 0,
                'message' => 'Please activate your account before trying to log in'
            );
        }
        
        echo json_encode($return);
    }

    /**
     * Logout user
     */
    public function logout(){

        Sentinel::logout();
        echo json_encode(array(
            'status' => 1,
            'message' => 'You are logged out.'
        ));

    }
}