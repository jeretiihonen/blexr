<?php
/**
 * this is the index controller for the system; here we handle the first initial call to the system, with blank parameters
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

/**
 * Import the necessary classes
 */
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class UserController
 */
class UserController extends ControllerBase {

    private $_Registry = null;
    private $_UserModel;

    /**
     * UserController constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;
        $this->_UserModel = new UserModel($Registry);
    }

    /**
     * Get list of users
     *
     * @return string
     */
    public function index() {
        if ( Sentinel::check() and Sentinel::inRole('admin') ) { // only for admin

            $role = Sentinel::findRoleBySlug('user');
            $users = $role->users()->with('roles')->get();

            foreach ($users as $user) {
                $user->features = $this->_Registry->Features->getUserFeatures($user->id);
            }

            $vars = array(
                'users' => $users,
                'user' => Sentinel::check()
            );

            echo json_encode(array(
                'status' => 1,
                'content' => $this->_Registry->Template->twig->render('user/list.html.twig', $vars)
            ));
        }
    }

    /**
     * Get add user form
     */
    public function form(){
        if ( Sentinel::check() != false and Sentinel::inRole('admin') ){
            echo json_encode(array(
                'status' => 1,
                'content' => $this->_Registry->Template->twig->render('user/addform.html.twig')
            ));
        }
    }

    /**
     * save user
     */
    public function save(){
        if ( Sentinel::check() != false and Sentinel::inRole('admin') ){
            $userDetails = Array(
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email']
            );
            $this->_UserModel->save($userDetails);

            echo json_encode(array(
                'status' => 1,
                'message' => 'New user added.'
            ));
        }
    }

    /**
     * Remove or add feature to user
     */
    public function saveFeature(){
        if ( Sentinel::check() != false and Sentinel::inRole('admin') ) {

            $userId = filter_input(INPUT_POST, 'userId', FILTER_SANITIZE_NUMBER_INT);
            $featureId = filter_input(INPUT_POST, 'featureId', FILTER_SANITIZE_NUMBER_INT);

            if ( $_POST['checked'] == 'true' ) {
                $this->_Registry->Features->saveUserFeature($userId, $featureId);
            } else {
                $this->_Registry->Features->removeUserFeature($userId, $featureId);
            }
        }
    }
    
}