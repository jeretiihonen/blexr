<?php
/**
 * this is the index controller for the system; here we handle the first initial call to the system, with blank parameters
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

/**
 * Class IndexController
 */
class IndexController extends ControllerBase {

    private $_Registry = null;

    /**
     * IndexController constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;
    }

    /**
     * Output site basic content
     */
    public function index() {
        echo $this->_Registry->Template->twig->render('index/index.html.twig');
    }


}