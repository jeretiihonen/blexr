<?php
/**
 * Import the necessary classes
 */
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class ContentController
 */
class ContentController {

    private $_Registry = null;

    /**
     * ContentController constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;
    }

    /**
     * returns content based user role, or login form if user not logged in
     *
     * @return string
     */
    public function index(){

        if ( $user = Sentinel::check() ){ // user is logged in

            $vars = array(
                'user' => $user
            );

            if ( Sentinel::inRole('admin') ){ // Show admin content

                $return = array(
                    'status' => 1,
                    'content' => $this->_Registry->Template->twig->render('index/content_admin.html.twig', $vars)
                );

            } else { // show users content

                $return = array(
                    'status' => 1,
                    'content' => $this->_Registry->Template->twig->render('index/content_user.html.twig', $vars)
                );
            }
        } else { // show login form

            $return = array(
                'status' => 1,
                'content' => $this->_Registry->Template->twig->render('index/login.html.twig')
            );

        }

        echo json_encode($return);

    }

}