<?php
/**
 * Import the necessary classes
 */
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class RequestController
 */
class RequestController {

    private $_Registry = null;
    private $_RequestModel;

    /**
     * RequestController constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;
        $this->_RequestModel = new RequestModel($Registry);
    }

    /**
     * Get request list for admin or user
     */
    public function index(){
        if ( Sentinel::inRole('admin') ){ // get not approved request for admin
            
            $vars = array(
                'requests' => $this->_RequestModel->fetchNewRequests(),
                'admin' => ( Sentinel::inRole('admin') ) ? true : false
            );

            $return = array(
                'status' => 1,
                'content' => $this->_Registry->Template->twig->render('requests/list.html.twig', $vars)
            );

        } else { // get users requests

            $user = Sentinel::check();
            $vars = array(
                'requests' => $this->_RequestModel->fetchUserRequests($user->id),
                'admin' => ( Sentinel::inRole('admin') ) ? true : false
            );

            $return = array(
                'status' => 1,
                'content' => $this->_Registry->Template->twig->render('requests/list.html.twig', $vars)
            );

        }

        echo json_encode($return);
    }

    /**
     * Show add user form
     */
    public function form(){
        echo json_encode(array(
            'status' => 1,
            'content' => $this->_Registry->Template->twig->render('requests/addform.html.twig')
        ));
    }

    /**
     * Save users request
     */
    public function save(){
        $requestType = filter_input(INPUT_POST, 'requestType', FILTER_SANITIZE_STRING);
        $this->_RequestModel->save($requestType);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Request sent.'
        ));
    }

    /**
     * handle request approval
     */
    public function handle(){
        $requestId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $requestStatus = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_NUMBER_INT);
        $response = $this->_RequestModel->saveStatus($requestId, $requestStatus);
        if ( $response == true ){

            $request = $this->_RequestModel->fetchRequest($requestId);
            $user = Sentinel::findById($request->userId);

            if ( $requestStatus == 0 ) { // rejected
                $requestStatusTxt = 'rejected';
            } else {
                $requestStatusTxt = 'approved';
            }

            mail($user->email, "Your request is ". $requestStatusTxt, "Hi ".$user->first_name .".\n\rYour request was ". $requestStatusTxt);

            $return = array(
                'status' => 1,
                'message' => 'Request '. $requestStatusTxt
            );

        } else {

            $return = array(
                'status' => 0,
                'message' => 'Error to handling users request'
            );

        }

        echo json_encode($return);
    }

    /**
     * Cacnel users request
     */
    public function delete(){

        $requestId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $response = $this->_RequestModel->delete($requestId);
        if ( $response == false ) {
            $return = array(
                'status' => 1,
                'message' => 'Request canceled'
            );
        } else {
            $return = array(
                'status' => 1,
                'message' => 'Request canceled'
            );
        }

        echo json_encode($return);
    }

}