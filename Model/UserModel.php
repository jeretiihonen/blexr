<?php

// Import the necessary classes
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class UserModel
 */
class UserModel extends ModelBase {

    private $_Registry = null;

    /**
     * UserModel constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        parent::__construct($Registry);
        $this->_Registry = $Registry;

    }

    /**
     * save and activate new user
     * @param array $data
     * @return array
     */
    public function save($data) {

        if (Sentinel::findByCredentials([
            'login' => $data['email'],
        ])) {
            return array(
                'status' => 0,
                'message' => 'User already exists with this email.'
            );
        }
        $password = $this->randomPassword();
        $role = Sentinel::findRoleByName('User');
        $user = Sentinel::registerAndActivate([
            'first_name' => $data['firstname'],
            'last_name' => $data['lastname'],
            'email' => $data['email'],
            'password' => $password,
            'permissions' => [
                'user.delete' => 0,
            ],
        ]);

        // attach the user to the role
        $role->users()->attach($user);

        // send password via email to user
        mail($data['email'], "Your account information", "Hi ".$data['firstname'] .".\n\rHere is your login to site:\n\rLoginname: ". $data['email'] ."\r\nPassword: ". $password);

        return true;
        
    }
    
    public function delete($args) {
        
    }

    /**
     * Generate random password for user
     * @return string
     */
    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
}