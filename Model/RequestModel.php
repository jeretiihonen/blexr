<?php

/**
 * Import the necessary classes
 */
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class RequestModel
 */
class RequestModel extends ModelBase {

    private $_Registry = null;

    /**
     * RequestModel constructor.
     * @param $Registry
     */
    public function __construct($Registry) {
        $this->_Registry = $Registry;
    }

    /**
     * @param array $requestType
     * @return bool
     */
    public function save($requestType){
        $user = Sentinel::check();
        $sql = "INSERT INTO requests (userId, requestType, requestDate) VALUES (". $user->id .", '$requestType', '". date("Y-m-d H:i:s", time()) ."')";
        $response = $this->_Registry->Database->getConnection()->exec($sql);
        if ( $response == false ){
            /*
             * todo: better error handling
             */
            return false;
            //print_r($this->_Registry->Database->getConnection()->errorInfo());
        }
        return true;
    }

    public function delete($id){

        $sql = "DELETE FROM requests WHERE id = $id";
        $response = $this->_Registry->Database->getConnection()->exec($sql);
        if ( $response == false ){
            /*
             * todo: better error handling
             */
            return false;
            //print_r($this->_Registry->Database->getConnection()->errorInfo());
        }
        return true;

    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    public function saveStatus($id, $status){

        $sql = "UPDATE requests SET approved=$status WHERE id = $id";
        $response = $this->_Registry->Database->getConnection()->exec($sql);
        if ( $response == false ){
            /*
             * todo: better error handling
             */
            return false;
            //print_r($this->_Registry->Database->getConnection()->errorInfo());
        }
        return true;
        
    }

    public function fetchRequest($id){

        $sql = "SELECT * FROM requests WHERE id=". $id;
        $result = $this->_Registry->Database->getConnection()->query($sql);
        if ( $result == false ){
            //print_r($this->_Registry->Database->getConnection()->errorInfo());
            return false;
        }
        return $result->fetch(PDO::FETCH_OBJ);

    }

    public function fetchNewRequests(){

        $sql = "
          SELECT 
            t1.*,
            t2.first_name,
            t2.last_name
          FROM 
            requests as t1
             LEFT JOIN users as t2 ON t1.userId = t2.id
          WHERE 
            approved IS NULL
        ";
        $result = $this->_Registry->Database->getConnection()->query($sql);
        if ( $result == false ){
            //print_r($this->_Registry->Database->getConnection()->errorInfo());
            return false;
        }
        return $result->fetchAll();

    }

    /**
     * Fetch all possible request from database
     * @return array
     */
    public function fetchAllRequests(){

        return;

        $sql = "SELECT * FROM enabled_features ";
        $result = $this->_Registry->Database->getConnection()->query($sql);
        $return = array();

        while($feature = $result->fetch(PDO::FETCH_OBJ)) {
            $return[$feature->id] = $feature->name;
        }

        return $return;

    }

    /**
     * Fetch all features what user has
     *
     * @param $userId
     * @return array
     */
    public function fetchUserRequests($userId){

        $sql = "SELECT * FROM requests WHERE userId = ". $userId;
        $result = $this->_Registry->Database->getConnection()->query($sql);
        if ( $result == false ){
            return false;
        }
        return $result->fetchAll();

    }

}