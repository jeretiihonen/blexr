/**
 * Start application
 */
$(function() {
    content.get();
});

/**
 *
 * @type {{login: auth.login, logout: auth.logout}}
 */
var auth = {

    /**
     * Try to login
     */
    login: function(){

        var username = $("input[name='login']").val();
        var password = $("input[name='password']").val();

        if ( username != '' && password != '' ){

            $.ajax({
                type: "POST",
                url: "/Auth/login",
                dataType: 'json',
                data: {
                    username : username,
                    password : password
                },
                success: function(response){

                    if ( response.status == '0' ){
                        alert(response.message);
                    } else {
                        content.get();
                    }
                }
            });
        }

    },

    /**
     * Logout user
     */
    logout: function(){

        $.ajax({
            url: "/Auth/logout",
            success: function(){
                content.get();
            }
        });

    }

}

/**
 * Content methods
 *
 * @type {{get: content.get, initButtons: content.initButtons}}
 */
var content = {

    /**
     * Get page content
     */
    get: function(){

        $.ajax({
            url: "/Content",
            dataType: 'json',
            success: function(response){
                $('#siteContainer').html(response.content);
                content.init();
            }
        });

    },

    /**
     * Init content
     */
    init: function(){

        if ( $('.requestsList').length == 0 ) {
            request.getList();
        }

        // init login button
        if ( $('.formLogin .button.send').length ) {

            // check if enter is pressed and send login
            $(".formLogin").on("keypress", ":input", function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    auth.login();
                }
            });

            // send login
            $('.formLogin .button.send').off('click').on('click', function (e) {
                auth.login();
            });
        }

        // init logoutbutton
        if ( $('#menuContainer .button.logout').length ) {
            $('#menuContainer .button.logout').off('click').on('click', function (e) {
                e.preventDefault();
                auth.logout();
            });
        }

        /**
         * if admin is in da house.
         * todo: move these to separated file
         */
        if ( $('#menuContainer .button.adduser').length ) {

            if ( $('.userList').length == 0 ){
                user.getList()
            }

            $('#menuContainer .button.adduser').off('click').on('click', function (e) {
                e.preventDefault();
                user.getForm();
            });

            $('.userFeatures input').off('change').on('change', function(){
                user.setFeatures(this);
            });

            if ( $('.requestItem .button.approve').length ) {

                $('.requestItem .button.approve').off('click').on('click', function (e) {
                    e.preventDefault();
                    request.handleRequest(this, 1);
                });

                $('.requestItem .button.reject').off('click').on('click', function (e) {
                    e.preventDefault();
                    request.handleRequest(this, 0);
                });
            }

            if ( $('.userList .button.features').length ){

                $('.userList .button.features').off('click').on('click', function (e) {
                    e.preventDefault();
                    $(this).parent().find('.userFeatures').toggle();
                });

            }
        } else {

            // init add request button
            if ( $('#menuContainer .button.addrequest').length ) {
                $('#menuContainer .button.addrequest').off('click').on('click', function (e) {
                    e.preventDefault();
                    request.getForm();
                });
            }

            // init cancel request button
            if ( $('.requestItem .button.cancel').length ) {
                $('.requestItem .button.cancel').off('click').on('click', function (e) {
                    e.preventDefault();
                    request.cancel(this);
                });
            }

        }
    }

}

/**
 *
 * @type {{getForm: request.getForm, save: request.save, cancel: request.cancel, handleRequest: request.handleRequest}}
 */
var request = {

    /**
     * Get list of request
     */
    getList: function(){

        $.ajax({
            type: "POST",
            url: "/Request",
            dataType: 'json',
            success: function (response) {
                $('.requestListContainer').html(response.content);
                content.init();
            }
        });

    },
    
    /**
     * Get form to addnew request
     */
    getForm: function(){
        if ( $('.addRequestForm').length == 0) {
            $.ajax({
                type: "POST",
                url: "/Request/form",
                dataType: 'json',
                success: function (response) {
                    $('.contentContainer').prepend(response.content);
                    // save
                    $('.addRequestForm .button.save').off('click').on('click', function (e) {
                        e.preventDefault();
                        request.save();
                    });
                    // cancel
                    $('.addRequestForm .button.cancel').off('click').on('click', function (e) {
                        e.preventDefault();
                        request.close();
                    });
                }
            });
        }
    },

    /**
     * Save request
     */
    save: function(){
        $.ajax({
            type: "POST",
            url: "/Request/save",
            dataType: 'json',
            data: {
                requestType : $("select[name='requestType'] option:selected").text()
            },
            success: function(response){
                alert(response.message);
                if ( response.status == 1 ) {
                    request.close();
                    request.getList();
                }
            }
        });
    },

    /**
     * Remove add request form
     */
    close: function(){
        $('.addRequestForm').remove();
    },

    /**
     * cancel users request
     */
    cancel: function(obj){

        $.ajax({
            type: "POST",
            url: "/Request/delete",
            dataType: 'json',
            data: {
                id : $(obj).closest('.requestItem').attr('data-requestid'),
            },
            success: function(response){
                alert(response.message);
                if ( response.status == 1 ) {
                    request.getList();
                }
            }
        });

    },

    /**
     * Approve or reject request
     */
    handleRequest: function(obj, status){

        $.ajax({
            type: "POST",
            url: "/Request/handle",
            dataType: 'json',
            data: {
                id : $(obj).closest('.requestItem').attr('data-requestid'),
                status : status
            },
            success: function(response){
                alert(response.message);
                if ( response.status == 1 ){
                    request.getList();
                }
            }
        });

    }

}

/**
 *
 * @type {{id: number, getForm: user.getForm, save: user.save, cancel: user.cancel}}
 */
var user = {

    /**
     * Get list of users
     */
    getList: function(){

        $.ajax({
            type: "POST",
            url: "/User",
            dataType: 'json',
            success: function (response) {
                $('.userListContainer').html(response.content);
                content.init();
            }
        });

    },

    /**
     * Get add new user form
     */
    getForm: function(){
        if ( $('.addUserForm').length == 0) {
            $.ajax({
                type: "POST",
                url: "/User/form",
                dataType: 'json',
                success: function (response) {
                    $('.contentContainer').prepend(response.content);
                    // bing save event
                    $('.addUserForm .button.save').off('click').on('click', function (e) {
                        e.preventDefault();
                        user.save();
                    });
                    // bing cancel event
                    $('.addUserForm .button.cancel').off('click').on('click', function (e) {
                        e.preventDefault();
                        user.close();
                    });
                }
            });
        }
    },

    /**
     * Save new user
     */
    save: function() {
        $.ajax({
            type: "POST",
            url: "/User/save",
            dataType: 'json',
            data: {
                firstname : $("input[name='firstname']").val(),
                lastname : $("input[name='lastname']").val(),
                email: $("input[name='email']").val()
            },
            success: function(response){
                alert(response.message);
                if ( response.status == 1 ) {
                    user.close();
                    user.getList();
                }
            }
        });
    },

    /**
     * Close add user form
     */
    close: function(){
        $('.addUserForm').remove();
    },

    /**
     * set user features
     * @param obj
     */
    setFeatures: function(obj){

        $.ajax({
            type: "POST",
            url: "/User/savefeature",
            dataType: 'json',
            data: {
                userId : $(obj).closest('.userItem').attr('data-userid'),
                checked : $(obj).prop("checked"),
                featureId: $(obj).val()
            },
            success: function(response){
                alert(response);
            }
        });

    }
}